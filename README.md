# Laravel on Kubernetes DO tutorial

[read here](https://www.digitalocean.com/community/tutorials/how-to-deploy-laravel-7-and-mysql-on-kubernetes-using-helm)

Follow the tutorial until step 4 but to make it work use ARM64 images for
mysql and httpd. and use the lamp/lamp helm chart [here](https://artifacthub.io/packages/helm/cloudnativeapp/lamp)

## Project setup

let's set up a docker file

### Docker file

```Dockerfile
# Use arm64 image to deploy on raspberry pi apache 
FROM arm64v8/php:8.0-apache 
# Install packages
RUN apt-get update 
RUN apt-get install -y zip
RUN apt-get install -y unzip
RUN apt-get install -y libbz2-dev
# Apache configuration
ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN a2enmod rewrite headers

# Common PHP Extensions
RUN docker-php-ext-install \
    bz2 \
    bcmath \
    pdo_mysql

# Ensure PHP logs are captured by the container
ENV LOG_CHANNEL=stderr

# Set a volume mount point for your code
VOLUME /var/www/html

# Copy code and run composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
COPY . /var/www/tmp
RUN cd /var/www/tmp && composer install --no-dev

# Ensure the entrypoint file can be run
RUN chmod +x /var/www/tmp/docker-entrypoint.sh
ENTRYPOINT ["/var/www/tmp/docker-entrypoint.sh"]

# The default apache run command
CMD ["apache2-foreground"]
```

### Build the image and push it to dockerhub

```bash
docker image build --push -t pacificdev/laravel-kubernetes:arm-latest .

```

## Helm chart configuration

Create a helm/ folder and put inside

helm/values.yml

```yml
php:
  repository: "pacificdev/laravel-kubernetes" #<-- the laravel docker image we created earlier 
  tag: "arm-latest" # <-- select the ARM build tag
  fpmEnabled: false
  envVars:
    - name: APP_ENV
      value: production
    - name: APP_DEBUG
      value: false
    - name: DB_PORT
      value: 3306
    - name: DB_HOST
      value: localhost

mysql: # <-- setup a different repo for the chart mysql database
  repository: "mariadb" # <-- use maria db latest docker image 
  tag: "latest" # that is compatible with arm64

httpd: # <-- set up a different repo for the chart apache server 
  repository: "arm64v8/httpd" # <-- use arm64 compatible apache image
  tag: "latest"

service:
  type: NodePort # <-- configure the service to run on nodePort

persistence: # <-- configure data persistence using hostPath
  size: 1Gi # assign a 1Gb storage space on the node
  hostPath: /home/ubuntu/laravel-kubernetes # <-- create this folder on a cluster node 
```

NOTE: Need to figure out how to use a network attached storage path instead of a folder inside the node.

create a file where store sensitive data like password and db details
inside helm/secrets.yml

```yml
mysql:
  rootPassword: "secret"
  user: fabio
  password: "secret"
  database: laravel_prod

php:
  envVars:
    - name: APP_KEY
      value: "base64:3bdYu2a1OLvMfcEeStw9LrYfby23vjCjogxtPOE2clo="
    - name: DB_DATABASE
      value: laravel_prod
    - name: DB_USERNAME
      value: fabio
    - name: DB_PASSWORD
      value: "secret"

```

## Install the helm chart

Now that all settings have been done we can install the helm chart using the values and secrets we defined earlier.

```bash

helm install laravel-kubernetes -f helm/values.yml -f helm/secrets.yml lamp/lamp
```

## Delete the chart

```bash
helm delete laravel-kubernetes
```

## Work with Laravel
